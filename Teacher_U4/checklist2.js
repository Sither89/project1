function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}
let user = getParameterByName('user'); // get value from param 'id'
let userStu = getParameterByName('userStu');
let subCode = getParameterByName('subCode');
function loadDoc() {
    craeteUI();
    const xmhttp = new XMLHttpRequest();
    xmhttp.onload = function () {
        const myObj = JSON.parse(this.responseText);
        for (let i = 0; i < Object.keys(myObj).length; i++) {
            if (myObj[i].username == userStu) {
                document.getElementById("date").innerHTML = myObj[i].date;
                document.getElementById("nname").innerHTML = myObj[i].nname;
                document.getElementById("fname").innerHTML = myObj[i].fname;
                document.getElementById("lname").innerHTML = myObj[i].lname;
                document.getElementById("idStu").innerHTML = myObj[i].username;
                document.getElementById("year").innerHTML = myObj[i].studentYear;
                document.getElementById("major").innerHTML = myObj[i].major;
                document.getElementById("advisor").innerHTML = myObj[i].advisor;
                document.getElementById("noHome").innerHTML = myObj[i].noHome;
                document.getElementById("swine").innerHTML = myObj[i].swine;
                document.getElementById("district").innerHTML = myObj[i].district;
                document.getElementById("districts").innerHTML = myObj[i].districts;
                document.getElementById("county").innerHTML = myObj[i].country;
                document.getElementById("zip").innerHTML = myObj[i].zip;
                document.getElementById("noPhone").innerHTML = myObj[i].noPhone;
                document.getElementById("email").innerHTML = myObj[i].email;
                document.getElementById("cause").innerHTML = myObj[i].cause;
            }
        }
    }
    xmhttp.open("GET", "/getUserStu");
    xmhttp.send();
}

function mainpageLogo() {
    var targeturl = window.location.pathname;
    targeturl = targeturl.replace('teacherStuInfo', 'Teacher') + '?' + 'user=' + user;
    window.location.href = targeturl;
}

function craeteUI() {
    Row();
    const xmhttp = new XMLHttpRequest();
    xmhttp.onload = function () {
        const myObj = JSON.parse(this.responseText);
        for (let i = 0; i < myObj.length; i++) {
            if (myObj[i].username == user) {
                for (let j = 0; j < 3; j++) {
                    if (j == 0) {
                        var query = myObj[i].username;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('teacherStuInfo', 'teacherStuList') + '?' + 'user=' + query;
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-envelope-open-text');
                        link.href = targeturl;
                        var Name = document.createTextNode("รายชื่อนักศึกษา");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    } else if (j == 1) {
                        var query = myObj[i].username;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('teacherStuInfo', 'Teacher_register_u3') + '?' + 'user=' + query;
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-user-edit');
                        link.href = targeturl;
                        var Name = document.createTextNode("อาจารย์กรอกข้อมูลติดต่อ");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    } else if (j == 2) {
                        var query = myObj[i].username;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('teacherStuInfo', 'login');
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-sign-out-alt');
                        link.href = targeturl;
                        var Name = document.createTextNode("ออกจากระบบ");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    }
                }
            }
        }
    }
    xmhttp.open("GET", "/getUserTeacher");
    xmhttp.send();
}

function saveFile(myjson) {
    const xhttp = new XMLHttpRequest();
    xhttp.onload = function () {
        let msg = this.responseText;
        alert(msg)
    };
    xhttp.open("POST", "/updating");
    xhttp.send(myjson);
}

function addStatusAp() {
    const xmhttp = new XMLHttpRequest();
    xmhttp.onload = function () {
        let json = this.responseText;
        const studentlist = JSON.parse(json);

        for (let i = 0; i < studentlist.length; i++) {
            if (studentlist[i].username == userStu) {
                for (let j = 0; j < studentlist[i].addsubjectList.length; j++) {
                    if (studentlist[i].addsubjectList[j].subjectCode == subCode)
                        studentlist[i].addsubjectList[j].subjectTeacherStatus = "Success";
                }
            }
        }
        var myJSON = JSON.stringify(studentlist);
        saveFile(myJSON)
        console.log(myJSON)
        window.location.reload();
    }

    xmhttp.open("GET", "/getUserStu");
    xmhttp.send();
}

function addStatusUa() {
    const xmhttp = new XMLHttpRequest();
    xmhttp.onload = function () {
        let json = this.responseText;
        const studentlist = JSON.parse(json);

        for (let i = 0; i < studentlist.length; i++) {
            if (studentlist[i].username == userStu) {
                for (let j = 0; j < studentlist[i].addsubjectList.length; j++) {
                    if (studentlist[i].addsubjectList[j].subjectCode == subCode)
                        studentlist[i].addsubjectList[j].subjectTeacherStatus = "Fail";
                }

            }
        }
        var myJSON = JSON.stringify(studentlist);
        saveFile(myJSON)
        console.log(myJSON)
        window.location.reload();
    }
    xmhttp.open("GET", "/getUserStu");
    xmhttp.send();

}

function Row() {
    var request = new XMLHttpRequest();
    request.open("GET", "/getUserTeacher", false);
    request.send(null)
    var teacherData = JSON.parse(request.responseText);
    var teacher;
    console.log(teacherData);
    for (let i = 0; i < teacherData.length; i++) {
        if (teacherData[i].username == user) {
            teacher = teacherData[i];
        }
    }

    var parentTable = document.getElementById('tableSub');
    var myTd, myP;
    var myTr;
    request.open("GET", "/getUserStu", false);
    request.send(null)
    var studentData = JSON.parse(request.responseText);
    for (let i = 0; i < studentData.length; i++) {
        for (let j = 0; j < studentData[i].addsubjectList.length; j++) {
            if (studentData[i].addsubjectList[j].subjectCode == subCode) {
                myTr = document.createElement('tr');
                for (let z = 0; z < 6; z++) {
                    if (z == 0) {
                        myTd = document.createElement('td');
                        myTd.setAttribute('class', 'myPCSS');
                        myTd.setAttribute('id', j);
                        myP = document.createTextNode(studentData[i].addsubjectList[j].subjectCode);
                        myTd.appendChild(myP);
                        myTr.appendChild(myTd);
                    } else if (z == 1) {
                        myTd = document.createElement('td');
                        myTd.setAttribute('class', 'myPCSS');
                        myP = document.createTextNode(studentData[i].addsubjectList[j].subjectName);
                        myTd.appendChild(myP);
                        myTr.appendChild(myTd);
                    } else if (z == 2) {
                        myTd = document.createElement('td');
                        myTd.setAttribute('class', 'myPCSS');
                        myP = document.createTextNode(studentData[i].addsubjectList[j].subjectSection);
                        myTd.appendChild(myP);
                        myTr.appendChild(myTd);
                    } else if (z == 3) {
                        myTd = document.createElement('td');
                        myTd.setAttribute('class', 'myPCSS');
                        myP = document.createTextNode(studentData[i].addsubjectList[j].subjectDate);
                        myTd.appendChild(myP);
                        myTr.appendChild(myTd);
                    } else if (z == 4) {
                        myTd = document.createElement('td');
                        myTd.setAttribute('class', 'myPCSS');
                        myP = document.createTextNode(studentData[i].addsubjectList[j].subjectCredit);
                        myTd.appendChild(myP);
                        myTr.appendChild(myTd);
                    } else if (z == 5) {
                        myTd = document.createElement('td');
                        myTd.setAttribute('class', 'myPCSS');
                        myP = document.createTextNode(studentData[i].addsubjectList[j].subjectTeacher);
                        myTd.appendChild(myP);
                        myTr.appendChild(myTd);
                    }
                }
            }
        }
    }
    parentTable.appendChild(myTr);
}