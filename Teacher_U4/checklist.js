var datajson;

function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}
let user = getParameterByName('user'); // get value from param 'id'


function craeteUI() {
    loadDoc();
    const xmhttp = new XMLHttpRequest();
    xmhttp.onload = function () {
        const myObj = JSON.parse(this.responseText);
        for (let i = 0; i < myObj.length; i++) {
            if (myObj[i].username == user) {
                for (let j = 0; j < 3; j++) {
                    if (j == 0) {
                        var query = myObj[i].username;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('teacherStuList', 'teacherStuList') + '?' + 'user=' + query;
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-envelope-open-text');
                        link.href = targeturl;
                        var Name = document.createTextNode("รายชื่อนักศึกษา");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    } else if (j == 1) {
                        var query = myObj[i].username;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('teacherStuList', 'Teacher_register_u3') + '?' + 'user=' + query;
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-user-edit');
                        link.href = targeturl;
                        var Name = document.createTextNode("อาจารย์กรอกข้อมูลติดต่อ");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    } else if (j == 2) {
                        var query = myObj[i].username;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('teacherStuList', 'login');
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-sign-out-alt');
                        link.href = targeturl;
                        var Name = document.createTextNode("ออกจากระบบ");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    }
                }
            }
        }
    }
    xmhttp.open("GET", "/getUserTeacher");
    xmhttp.send();
}

function loadDoc() {
    var request = new XMLHttpRequest();
    request.open("GET", "/getUserTeacher", false);
    request.send(null)
    var teacherData = JSON.parse(request.responseText);
    var teacher;
    console.log(teacherData);
    for (let i = 0; i < teacherData.length; i++) {
        if (teacherData[i].username == user) {
            teacher = teacherData[i];
        }
    }
    console.log(teacher);
    var query;
    var parentTable = document.getElementById('Table');
    var myTd, myP;
    var myTr = document.createElement('tr');
    request.open("GET", "/getUserStu", false);
    request.send(null)
    var studentData = JSON.parse(request.responseText);
    for (let i = 0; i < studentData.length; i++) {
        for (let j = 0; j < studentData[i].addsubjectList.length; j++) {
            if (studentData[i].addsubjectList[j].subjectTeacher == teacher.name && studentData[i].addsubjectList[j].subjectTeacherStatus == "Waiting" ) {
                myTr = document.createElement('tr');
                query = studentData[i].username;
                for (let z = 0; z < 4; z++) {
                    if (z == 0) {
                        myTd = document.createElement('td');
                        myP = document.createElement('p');
                        myP = document.createTextNode(studentData[i].fname + "  " + studentData[i].lname);
                        myTd.appendChild(myP);
                        myTr.appendChild(myTd);
                    } else if (z == 1) {
                        myTd = document.createElement('td');
                        myP = document.createElement('p');
                        myP = document.createTextNode(studentData[i].addsubjectList[j].subjectCode);
                        myTd.appendChild(myP);
                        myTr.appendChild(myTd);
                    } else if (z == 2) {
                        myTd = document.createElement('td');
                        myP = document.createElement('p');
                        myP = document.createTextNode(studentData[i].addsubjectList[j].subjectSection);
                        myTd.appendChild(myP);
                        myTr.appendChild(myTd);
                    } else {
                        myTd = document.createElement('td');
                        myP = document.createElement('a');
                        myP.href = window.location.href.replace('teacherStuList', 'teacherStuInfo') + "&userStu=" + query + "&subCode=" + studentData[i].addsubjectList[j].subjectCode;
                        var Perform = document.createTextNode(studentData[i].addsubjectList[j].subjectTeacherStatus);
                        myP.appendChild(Perform);
                        myTd.appendChild(myP);
                        myTr.appendChild(myTd);
                    }
                }
            }
            parentTable.appendChild(myTr);
        }
    }
    
}


function mainpageLogo() {
    var targeturl = window.location.pathname;
    targeturl = targeturl.replace('teacherStuList', 'Teacher') + '?' + 'user=' + user;
    window.location.href = targeturl;
}
