function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

let user = getParameterByName('user');// get value from param 'user'

function loadDoc() {
    craeteUI();
    const xmhttp = new XMLHttpRequest();
    xmhttp.onload = function () {
        const myObj = JSON.parse(this.responseText);
        for (let i = 0; i < myObj.length; i++) {
            if (myObj[i].username == user) {
                document.getElementById("div3").innerHTML = myObj[i].name;
            }
        }
    }
    xmhttp.open("GET", "/getUserTeacher");
    xmhttp.send();
}


function craeteUI() {
    const xmhttp = new XMLHttpRequest();
    xmhttp.onload = function () {
        const myObj = JSON.parse(this.responseText);
        for (let i = 0; i < myObj.length; i++) {
            if (myObj[i].username == user) {
                for (let j = 0; j < 3; j++) {
                    if (j == 0) {
                        var query = myObj[i].username;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('Teacher', 'teacherStuList') + '?' + 'user=' + query;
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-envelope-open-text');
                        link.href = targeturl;
                        var Name = document.createTextNode("รายชื่อนักศึกษา");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    }else if(j == 1){
                        var query = myObj[i].username;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('Teacher', 'Teacher_register_u3')  + '?' + 'user=' + query;;
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-user-edit');
                        link.href = targeturl;
                        var Name = document.createTextNode("อาจารย์กรอกข้อมูลติดต่อ");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    }else if(j == 2){
                        var query = myObj[i].username;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('Teacher', 'login');
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-sign-out-alt');
                        link.href = targeturl;
                        var Name = document.createTextNode("ออกจากระบบ");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    }
                }
            }
        }
    }
    xmhttp.open("GET", "/getUserTeacher");
    xmhttp.send();
}
function mainpageLogoTeacher(){
    var targeturl = window.location.pathname;
    targeturl = targeturl.replace('teacherStuList', 'Teacher') + '?' + 'user=' + user;
    window.location.href = targeturl;
}
