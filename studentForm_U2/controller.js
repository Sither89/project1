var count = 0;
var row = 1;
var countSub = 0;
function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

let user = getParameterByName('user');// get value from param 'user'

function mainpageLogo() {
    var targeturl = window.location.pathname;
    targeturl = targeturl.replace('enroll', 'student') + '?' + 'user=' + user;
    window.location.href = targeturl;
}

function craeteUI() {
    const xhttp = new XMLHttpRequest();
    let targetUrl = "https://restapi.tu.ac.th/api/v2/profile/std/info/?id=" + user;
    xhttp.open("GET", targetUrl);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.setRequestHeader("Application-Key", "TU2abb8a5ef28d6a54224794d359f3a46fb57bd3b8e1c836fee23ca3b30f91af1480d3e6b4799321ae57be9943904ba18d");
    xhttp.send();
    xhttp.onreadystatechange = function () {
        const myObj = JSON.parse(this.responseText);
        if (this.readyState == 4 && this.status == 200) {
            if (myObj.data.userName == user) {
                for (let j = 0; j < 5; j++) {
                    if (j == 0) {
                        var query = myObj.data.userName;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('enroll', 'enroll') + '?' + 'user=' + query;
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-envelope-open-text');
                        link.href = targeturl;
                        var Name = document.createTextNode("จดทะเบียนล่าช้า");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    } else if (j == 1) {
                        var query = myObj.data.userName;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('enroll', 'studentHis') + '?' + 'user=' + query;
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-history');
                        link.href = targeturl;
                        var Name = document.createTextNode("ประวัติการส่งคำร้อง");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    } else if (j == 2) {
                        var query = myObj.data.userName;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('enroll', 'studentCost') + '?' + 'user=' + query;     //แก้ลิงค์
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-dollar-sign');
                        link.href = targeturl;
                        var Name = document.createTextNode("ภาระค่าใช้จ่าย/ทุน");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    } else if (j == 3) {
                        var query = myObj.data.userName;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('enroll', 'Teacher_info_u3') + '?' + 'user=' + query;
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-envelope-open-text');
                        link.href = targeturl;
                        var Name = document.createTextNode("ข้อมูลติดต่ออาจารย์");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    } else if (j == 4) {
                        var query = myObj.data.userName;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('enroll', 'login');
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-sign-out-alt');
                        link.href = targeturl;
                        var Name = document.createTextNode("ออกจากระบบ");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    }
                }
            }
        }
        if (this.readyState != 4 && this.status != 200) {
            alert("Error");
        }
    }
}


var myObj = {
    "username": null,
    "date": null,
    "nname": null,
    "fname": null,
    "lname": null,
    "studentYear": null,
    "major": null,
    "advisor": null,
    "noHome": null,
    "swine": null,
    "district": null,
    "districts": null,
    "country": null,
    "zip": null,
    "noPhone": null,
    "cause": null,
    "email": null,
    "addsubjectList": [
        {
            "subjectCode": null,
            "subjectName": null,
            "subjectSection": null,
            "subjectDate": null,
            "subjectCredit": null,
            "subjectTeacher": null,
            "subjectTeacherStatus": null
        }
    ],
    "cost": null
};

function validateData() {
    let date = document.getElementById("date").value;
    let nname = document.getElementById("nname").value;
    let fname = document.getElementById("fname").value;
    let lname = document.getElementById("lname").value;
    let idStu = document.getElementById("idStu").value;
    let year = document.getElementById("year").value;
    let major = document.getElementById("major").value;
    let advisor = document.getElementById("advisor").value;
    let noHome = document.getElementById("noHome").value;
    let swine = document.getElementById("swine").value;
    let district = document.getElementById("district").value;
    let districts = document.getElementById("districts").value;
    let county = document.getElementById("county").value;
    let zip = document.getElementById("zip").value;
    let noPhone = document.getElementById("noPhone").value;
    let email = document.getElementById("email").value;
    let comment = document.getElementById("cause").value;
    if (date == "") {
        alert("กรุณากรอกวันที่");
        return false;
    } else if (nname == "") {
        alert("กรุณากรอกคำนำหน้าชื่อ");
        return false;
    } else if (fname == "") {
        alert("กรุณากรอกชื่อ");
        return false;
    } else if (lname == "") {
        alert("กรุณากรอกนามสกุล");
        return false;
    } else if (idStu == "") {
        alert("กรุณากรอกรหัสนักศึกษา");
        return false;
    } else if (idStu.length != 10) {
        alert("กรุณากรอกรหัสนักศึกษาให้ถูกต้อง");
        return false;
    } else if (year == "") {
        alert("กรุณาเลือกชั้นปีที่");
        return false;
    } else if (major == "") {
        alert("กรุณาเลือกสาขาวิชา");
        return false;
    } else if (noHome == "") {
        alert("กรุณากรอกบ้านเลขที่");
        return false;
    } else if (swine == "") {
        alert("กรุณากรอกหมู่");
        return false;
    } else if (district == "") {
        alert("กรุณากรอกตำบล");
        return false;
    } else if (districts == "") {
        alert("กรุณากรอกอำเภอ");
        return false;
    } else if (county == "") {
        alert("กรุณากรอกจังหวัด");
        return false;
    } else if (zip == "") {
        alert("กรุณากรอกรหัสไปรษณีย์");
        return false;
    } else if (zip.length != 5) {
        alert("กรุณากรอกรหัสไปรษณีย์ให้ถูกต้อง");
        return false;
    } else if (noPhone == "") {
        alert("กรุณากรอกเบอร์โทรศัพท์มือถือ");
        return false;
    } else if (noPhone.length != 10 || noPhone.charAt(0) != 0) {
        alert("กรุณากรอกเบอร์โทรศัพท์มือถือให้ถูกต้อง");
        return false;
    } else if (advisor == "") {
        alert("กรุณากรอกชื่ออาจารย์ที่ปรึกษา");
        return false;
    } else if (email == 0) {
        alert("กรุณากรอกอีเมลล์");
        return false;
    } else if (comment == "") {
        alert("กรุณากรอกเหตุผล");
        return false;
    } else if(count <1){
        alert("กรุณาเพิ่มรายวิชาอย่างน้อย 1 วิชา")
        return false;
    }else {
        addData();
        saveFile();

    }

}

function addData() {
    myObj.date = document.getElementById("date").value;
    myObj.nname = document.getElementById("nname").value;
    myObj.fname = document.getElementById("fname").value;
    myObj.lname = document.getElementById("lname").value;
    myObj.username = document.getElementById("idStu").value;
    myObj.studentYear = document.getElementById("year").value;
    myObj.major = document.getElementById("major").value;
    myObj.advisor = document.getElementById("advisor").value;
    myObj.noHome = document.getElementById("noHome").value;
    myObj.swine = document.getElementById("swine").value;
    myObj.district = document.getElementById("district").value;
    myObj.districts = document.getElementById("districts").value;
    myObj.country = document.getElementById("county").value;
    myObj.zip = document.getElementById("zip").value;
    myObj.noPhone = document.getElementById("noPhone").value;
    myObj.email = document.getElementById("email").value;
    myObj.cause = document.getElementById("cause").value;

    for (var i = 0; i < row - 2; i++) {
        var cloneaddObj = {
            "subjectCode": null,
            "subjectName": null,
            "subjectSection": null,
            "subjectDate": null,
            "subjectCredit": null,
            "subjectTeacher": null,
            "subjectTeacherStatus": null,
        }
        if (row > 1) {
            myObj.addsubjectList.push(cloneaddObj);
        }
    }
    var count = 1;
    for (var i = 0; i < row-1; i++) {
        var index = 10 * count + 1;
        console.log(index);
        myObj.addsubjectList[i].subjectCode = document.getElementById(index).innerHTML;
        myObj.addsubjectList[i].subjectName = document.getElementById(index + 1).innerHTML;
        myObj.addsubjectList[i].subjectSection = document.getElementById(index + 2).innerHTML;
        myObj.addsubjectList[i].subjectDate = document.getElementById(index + 3).innerHTML;
        myObj.addsubjectList[i].subjectCredit = parseInt(document.getElementById(index + 4).innerHTML);
        myObj.addsubjectList[i].subjectTeacher = document.getElementById(index + 5).innerHTML;
        myObj.addsubjectList[i].subjectTeacherStatus = document.getElementById(index + 6).innerHTML;
        count++;
    }
    console.log(myObj);
}

function saveFile() {
    const xhttp = new XMLHttpRequest();
    xhttp.onload = function () {
        let msg = this.responseText;
        alert(msg);
    }
    let json = JSON.stringify(myObj);
    xhttp.open("POST", "/saveStudent");
    xhttp.send(json);
}


function searchSubject() {
    let searchSubCode = document.getElementById("subjectCode").value;
    let searchSubName = document.getElementById("subjectName").value;
    if (searchSubName == '' && searchSubCode == '') {
        alert("กรุณากรอกคำค้นหารายวิชา");
        return false;
    } else {
        const xmhttp = new XMLHttpRequest();
        xmhttp.onload = function () {
            const myObj = JSON.parse(this.responseText);
            for (let i = 0; i < Object.keys(myObj).length; i++) {
                const indexCode = myObj[i].subjectCode.search(searchSubCode);
                const indexName = myObj[i].subjectName.search(searchSubName);
                if (indexCode != -1 && indexName != -1 && searchSubName != '' && searchSubCode != '') {
                    let parentDiv = document.getElementById("listSub");
                    let myP = document.createElement('p');
                    let confirmButton = document.createElement('button');
                    myP.setAttribute('id', 'Subject' + i);
                    parentDiv.appendChild(myP);
                    confirmButton.setAttribute('onclick', 'confirm("' + myObj[i].subjectCode + '")');
                    confirmButton.setAttribute('class', 'btn fa fa-check');
                    confirmButton.setAttribute('id', 'check');
                    document.getElementById('Subject' + i).innerHTML = myObj[i].subjectCode + "  " + myObj[i].subjectName + "  " + myObj[i].subjectSection + "  " + myObj[i].subjectDate;
                    myP.appendChild(confirmButton);
                    countSub++;
                } else if (indexCode != -1 && searchSubName == '' && searchSubCode != '') {
                    let parentDiv = document.getElementById("listSub");
                    let myP = document.createElement('p');
                    let confirmButton = document.createElement('button');
                    myP.setAttribute('id', 'Subject' + i);
                    parentDiv.appendChild(myP);
                    confirmButton.setAttribute('onclick', 'confirm("' + myObj[i].subjectCode + '")');
                    confirmButton.setAttribute('class', 'btn fa fa-check');
                    confirmButton.setAttribute('id', 'check');
                    document.getElementById('Subject' + i).innerHTML = myObj[i].subjectCode + "  " + myObj[i].subjectName + "  " + myObj[i].subjectSection + "  " + myObj[i].subjectDate;
                    myP.appendChild(confirmButton);
                    countSub++;
                } else if (indexName != -1 && searchSubCode == '' && searchSubName != '') {
                    let parentDiv = document.getElementById("listSub");
                    let myP = document.createElement('p');
                    let confirmButton = document.createElement('button');
                    myP.setAttribute('id', 'Subject' + i);
                    parentDiv.appendChild(myP);
                    confirmButton.setAttribute('onclick', 'confirm("' + myObj[i].subjectCode + '")');
                    confirmButton.setAttribute('class', 'btn fa fa-check');
                    confirmButton.setAttribute('id', 'check');
                    document.getElementById('Subject' + i).innerHTML = myObj[i].subjectCode + "  " + myObj[i].subjectName + "  " + myObj[i].subjectSection + "  " + myObj[i].subjectDate;
                    myP.appendChild(confirmButton);
                    countSub++;
                }else if (myObj[i].subjectCode != searchSubCode && myObj[i].subjectName != searchSubName){
                    alert("ไม่มีรายวิชาที่ค้นหา");
                    return false;
                }
            }
        }
        xmhttp.open("GET", "/getSubject");
        xmhttp.send();
    }
}

function confirm(subjectCode) {
    addRows(subjectCode);
    for (let x = 0; x < countSub; x++) {
        let myP = document.getElementById("Subject" + x);
        myP.parentNode.removeChild(myP);
    }
}

function addRows(subjectCode) {
    const xmhttp = new XMLHttpRequest();
    xmhttp.onload = function () {
        const myObj = JSON.parse(this.responseText);
        var parentTable = document.getElementById('tableSub');
        var myTd;
        var myTr = document.createElement('tr');
        for (j = 0; j < myObj.length; j++) {
            if (myObj[j].subjectCode == subjectCode) {
                for (var i = 0; i < 7; i++) {
                    if (i == 0) {
                        myTd = document.createElement('td');
                        myTd.setAttribute('id',10*row+1);
                        myp = document.createTextNode(myObj[j].subjectCode);
                        myTd.appendChild(myp);
                        myTr.appendChild(myTd);
                    } else if (i == 1) {
                        myTd = document.createElement('td');
                        myTd.setAttribute('id',10*row+2);
                        myp = document.createTextNode(myObj[j].subjectName);
                        myTd.appendChild(myp);
                        myTr.appendChild(myTd);
                    } else if (i == 2) {
                        myTd = document.createElement('td');
                        myTd.setAttribute('id',10*row+3);
                        myp = document.createTextNode(myObj[j].subjectSection);
                        myTd.appendChild(myp);
                        myTr.appendChild(myTd);
                    } else if (i == 3) {
                        myTd = document.createElement('td');
                        myTd.setAttribute('id',10*row+4);
                        myp = document.createTextNode(myObj[j].subjectDate);
                        myTd.appendChild(myp);
                        myTr.appendChild(myTd);
                    } else if (i == 4) {
                        myTd = document.createElement('td');
                        myTd.setAttribute('id',10*row+5);
                        myp = document.createTextNode(myObj[j].subjectCredit);
                        myTd.appendChild(myp);
                        myTr.appendChild(myTd);
                    }else if (i == 5) {
                        myTd = document.createElement('td');
                        myTd.setAttribute('id',10*row+6);
                        myp = document.createTextNode(myObj[j].subjectTeacher);
                        myTd.appendChild(myp);
                        myTr.appendChild(myTd);
                    }else if (i == 6) {
                        myTd = document.createElement('td');
                        myTd.setAttribute('id',10*row+7);
                        myp = document.createTextNode(myObj[j].subjectTeacherStatus);
                        myTd.appendChild(myp);
                        myTr.appendChild(myTd);
                    }
                }
                count++;
                row++;
            }
            parentTable.appendChild(myTr);
        }
    }
    xmhttp.open("GET", "/getSubject");
    xmhttp.send();
}

function removeTable1() {
    if (count > 0) {
        document.getElementById("tableSub").deleteRow(row - 1);
        count--;
        row--;
    }
}