const togglePassword = document.querySelector('#togglePassword');
const password = document.querySelector('#password');
togglePassword.addEventListener('click', function (e) {
    // toggle the type attribute
    const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
    password.setAttribute('type', type);
    // toggle the eye / eye slash icon
    this.classList.toggle('bi-eye');
});

const rmCheck = document.getElementById("remember"),
    username = document.getElementById("Username");

if (localStorage.checkbox && localStorage.checkbox !== "") {
    rmCheck.setAttribute("checked", "checked");
    username.value = localStorage.username;
} else {
    rmCheck.removeAttribute("checked");
    username.value = "";
}

function lsRememberMe() {
    if (rmCheck.checked && username.value !== "") {
        localStorage.username = username.value;
        localStorage.checkbox = rmCheck.value;
    } else {
        localStorage.username = "";
        localStorage.checkbox = "";
    }
}


function login() {
    const username = document.getElementById("Username").value;
    const password = document.getElementById("password").value;
    loginTeacher(username, password);
    loginStudent();
}

function loginStudent() {
    const username = document.getElementById("Username").value;
    const password = document.getElementById("password").value;
    const xhttp = new XMLHttpRequest();
    xhttp.open("POST", "https://restapi.tu.ac.th/api/v1/auth/Ad/verify");
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.setRequestHeader("Application-Key", "TU2abb8a5ef28d6a54224794d359f3a46fb57bd3b8e1c836fee23ca3b30f91af1480d3e6b4799321ae57be9943904ba18d");
    xhttp.send(JSON.stringify({ "UserName": username, "PassWord": password }));
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            lsRememberMe();
            const object = JSON.parse(xhttp.responseText);
            query = object.username;
            console.log(object);
            if (object.type == "student") {
                var targeturl = window.location.href;
                targeturl = targeturl.replace('login', 'student') + "?user=" + query;
                window.location.href = targeturl;
            }
        }
        if (this.readyState != 4 && this.status != 200) {
            var loginFail = document.getElementById("loginFail");
            loginFail.setAttribute('class', 'loginFail1');
            document.getElementById("loginFail").innerHTML = " !! Incorrect username or password !! ";
        }
    }
};


function loginTeacher(user, pass) {
    const xmhttp = new XMLHttpRequest();
    xmhttp.onload = function () {
        const myObj = JSON.parse(this.responseText);
        console.log(myObj);
        for (let i = 0; i < myObj.length; i++) {
            query = myObj[i].username;
            if (myObj[i].username == user && myObj[i].password == pass && myObj[i].role == "Teacher") {
                lsRememberMe();
                var targeturl = window.location.href;
                targeturl = targeturl.replace('login', 'Teacher') + '?user=' + query;
                window.location.href = targeturl;
            }
        }
    }
    xmhttp.open("GET", "/getUserTeacher");
    xmhttp.send();
}
