function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

let user = getParameterByName('user');// get value from param 'user'

function mainpageLogo() {
    var targeturl = window.location.pathname;
    targeturl = targeturl.replace('studentCost', 'student') + '?' + 'user=' + user;
    window.location.href = targeturl;
}

function InfoUser() {
    const xhttp = new XMLHttpRequest();
    let targetUrl = "https://restapi.tu.ac.th/api/v2/profile/std/info/?id=" + user;
    xhttp.open("GET", targetUrl);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.setRequestHeader("Application-Key", "TU2abb8a5ef28d6a54224794d359f3a46fb57bd3b8e1c836fee23ca3b30f91af1480d3e6b4799321ae57be9943904ba18d");
    xhttp.send();
    xhttp.onreadystatechange = function () {
        const myObj = JSON.parse(this.responseText);
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("div3").innerHTML = myObj.data.userName + ' : ' + myObj.data.displayname_th;
        }
        if (this.readyState != 4 && this.status != 200) {
            alert("Error");
        }
    }
}


function loadDoc() {
    createUI();
    InfoUser();
    var deadline;
    var send;
    var diffTime;
    var diffDays;
    var diffDaysM;
    var priceCre = 225;
    var parentTable = document.getElementById('Table');
    var myTd, myP;
    var count = 0;
    var myTr = document.createElement('tr');
    const xmhttp = new XMLHttpRequest();
    xmhttp.onload = function () {
        const myObj = JSON.parse(this.responseText);
        for (let i = 0; i < myObj.length; i++) {
            if (myObj[i].username == user) {
                myTr = document.createElement('tr');
                for (let j = 0; j < 3; j++) {
                    if (j == 0) {
                        myTd = document.createElement('td');
                        myP = document.createElement('p');
                        myP = document.createTextNode(myObj[i].date);
                        myTd.appendChild(myP);
                        myTr.appendChild(myTd);
                    } else if (j == 1) {
                        myTd = document.createElement('td');
                        myP = document.createElement('p');
                        myP = document.createTextNode("บำรุงมหาวิทยาลัย");
                        myTd.appendChild(myP);
                        myTr.appendChild(myTd);
                    } else if (j == 2) {
                        myTd = document.createElement('td');
                        myP = document.createElement('p');
                        myP = document.createTextNode(225);
                        myTd.appendChild(myP);
                        myTr.appendChild(myTd);
                    }
                }
                parentTable.appendChild(myTr);
                for (let z = 0; z < myObj[i].addsubjectList.length; z++) {
                    let date_deadline = "2021-11-19"
                    deadline = new Date(date_deadline);
                    send = new Date(myObj[i].date);
                    diffTime = Math.abs(send - deadline);
                    diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                    diffDaysM = diffDays * 45;
                    myTr = document.createElement('tr');
                    for (let j = 0; j < 3; j++) {
                        if (j == 0) {
                            myTd = document.createElement('td');
                            myP = document.createElement('p');
                            myP = document.createTextNode(myObj[i].date);
                            myTd.appendChild(myP);
                            myTr.appendChild(myTd);
                        } else if (j == 1) {
                            myTd = document.createElement('td');
                            myP = document.createElement('p');
                            myP = document.createTextNode(myObj[i].addsubjectList[z].subjectCode);
                            myTd.appendChild(myP);
                            myTr.appendChild(myTd);
                        } else if (j == 2) {

                            priceCre = priceCre + parseInt(1200 * myObj[i].addsubjectList[z].subjectCredit);

                            myTd = document.createElement('td');
                            myP = document.createElement('p');
                            myP = document.createTextNode(myObj[i].addsubjectList[z].subjectCredit * 1200);
                            myTd.appendChild(myP);
                            myTr.appendChild(myTd);
                        }
                    }
                    parentTable.appendChild(myTr);
                    count++;
                }
                myTr = document.createElement('tr');
                for (let j = 0; j < 3; j++) {
                    if (j == 0) {
                        myTd = document.createElement('td');
                        myP = document.createElement('p');
                        myP = document.createTextNode(myObj[i].date);
                        myTd.appendChild(myP);
                        myTr.appendChild(myTd);
                    } else if (j == 1) {
                        myTd = document.createElement('td');
                        myP = document.createElement('p');
                        myP = document.createTextNode("ค่าปรับในการจดทะเบียนล่าช้า");
                        myTd.appendChild(myP);
                        myTr.appendChild(myTd);
                    } else if (j == 2) {
                        myTd = document.createElement('td');
                        myP = document.createElement('p');
                        myP = document.createTextNode(diffDaysM);
                        myTd.appendChild(myP);
                        myTr.appendChild(myTd);
                    }
                }
                parentTable.appendChild(myTr);

            }
        }

        priceCre += diffDaysM;

        document.getElementById("val").innerHTML = "รวมเป็นเงิน = " + priceCre;

    }
    xmhttp.open("GET", "/getUserStu");
    xmhttp.send();
}

function createUI() {
    const xhttp = new XMLHttpRequest();
    let targetUrl = "https://restapi.tu.ac.th/api/v2/profile/std/info/?id=" + user;
    xhttp.open("GET", targetUrl);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.setRequestHeader("Application-Key", "TU2abb8a5ef28d6a54224794d359f3a46fb57bd3b8e1c836fee23ca3b30f91af1480d3e6b4799321ae57be9943904ba18d");
    xhttp.send();
    xhttp.onreadystatechange = function () {
        const myObj = JSON.parse(this.responseText);
        if (this.readyState == 4 && this.status == 200) {
            if (myObj.data.userName == user) {
                for (let j = 0; j < 5; j++) {
                    if (j == 0) {
                        var query = myObj.data.userName;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('studentCost', 'enroll') + '?' + 'user=' + query;
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-envelope-open-text');
                        link.href = targeturl;
                        var Name = document.createTextNode("จดทะเบียนล่าช้า");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    } else if (j == 1) {
                        var query = myObj.data.userName;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('studentCost', 'studentHis') + '?' + 'user=' + query;
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-history');
                        link.href = targeturl;
                        var Name = document.createTextNode("ประวัติการส่งคำร้อง");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    } else if (j == 2) {
                        var query = myObj.data.userName;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('studentCost', 'studentCost') + '?' + 'user=' + query;     //แก้ลิงค์
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-dollar-sign');
                        link.href = targeturl;
                        var Name = document.createTextNode("ภาระค่าใช้จ่าย/ทุน");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    } else if (j == 3) {
                        var query = myObj.data.userName;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('studentCost', 'Teacher_info_u3') + '?' + 'user=' + query;
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-envelope-open-text');
                        link.href = targeturl;
                        var Name = document.createTextNode("ข้อมูลติดต่ออาจารย์");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    } else if (j == 4) {
                        var query = myObj.data.userName;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('studentCost', 'login');
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-sign-out-alt');
                        link.href = targeturl;
                        var Name = document.createTextNode("ออกจากระบบ");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    }
                }
            }
        }
        if (this.readyState != 4 && this.status != 200) {
            alert("Error");
        }
    }
}