function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}
let user = getParameterByName('user');
let id = getParameterByName('id');

function mainpageLogo() {
    var targeturl = window.location.pathname;
    targeturl = targeturl.replace('studentHis', 'student') + '?' + 'user=' + user;
    window.location.href = targeturl;
}

function loadDoc() {
    craeteUI()
    // search student by name and year param and set hyperlink to info of student
    const xmhttp = new XMLHttpRequest();
    xmhttp.onload = function () {
        const myObj = JSON.parse(this.responseText);
        for (let i = 0; i < Object.keys(myObj).length; i++) {
            if (user == myObj[i].username && user != null) {
                for (let j = 0; j < myObj[i].addsubjectList.length; j++) {
                    var query = myObj[i].username;
                    var targeturl = window.location.pathname;
                    targeturl = targeturl.replace('studentHis', 'studentHisInfo') + '?' + 'user=' + query + "&sub=" + myObj[i].addsubjectList[j].subjectCode;
                    var parentUL = document.getElementById('subList');
                    var parentLI = document.createElement('li');
                    var link = document.createElement('a');
                    var p = document.createElement('p');
                    var k = document.createElement('p');
                    parentLI.setAttribute('id', 'sub1');
                    link.setAttribute('id', 'text1');
                    link.href = targeturl;
                    var Name = document.createTextNode(myObj[i].addsubjectList[j].subjectCode);
                    link.appendChild(Name);
                    p.setAttribute('id', 'date');
                    var date = document.createTextNode(myObj[i].date);
                    p.appendChild(date);
                    k.setAttribute('id', 'status');
                    var status = document.createTextNode(myObj[i].addsubjectList[j].subjectTeacherStatus);
                    k.appendChild(status);
                    var br = document.createElement('br');
                    parentLI.appendChild(link);
                    parentLI.appendChild(p);
                    parentLI.appendChild(k);
                    parentUL.appendChild(parentLI);
                    parentUL.appendChild(br);
                }
            }
        }
    }
    xmhttp.open("GET", "/getUserStu");
    xmhttp.send();
}


function craeteUI() {
    const xhttp = new XMLHttpRequest();
    let targetUrl = "https://restapi.tu.ac.th/api/v2/profile/std/info/?id=" + user;
    xhttp.open("GET", targetUrl);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.setRequestHeader("Application-Key", "TU2abb8a5ef28d6a54224794d359f3a46fb57bd3b8e1c836fee23ca3b30f91af1480d3e6b4799321ae57be9943904ba18d");
    xhttp.send();
    xhttp.onreadystatechange = function () {
        const myObj = JSON.parse(this.responseText);
        if (this.readyState == 4 && this.status == 200) {
            if (myObj.data.userName == user) {
                for (let j = 0; j < 5; j++) {
                    if (j == 0) {
                        var query = myObj.data.userName;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('studentHis', 'enroll') + '?' + 'user=' + query;
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-envelope-open-text');
                        link.href = targeturl;
                        var Name = document.createTextNode("จดทะเบียนล่าช้า");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    } else if (j == 1) {
                        var query = myObj.data.userName;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('studentHis', 'studentHis') + '?' + 'user=' + query;
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-history');
                        link.href = targeturl;
                        var Name = document.createTextNode("ประวัติการส่งคำร้อง");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    } else if (j == 2) {
                        var query = myObj.data.userName;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('studentHis', 'studentCost') + '?' + 'user=' + query;     //แก้ลิงค์
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-dollar-sign');
                        link.href = targeturl;
                        var Name = document.createTextNode("ภาระค่าใช้จ่าย/ทุน");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    } else if (j == 3) {
                        var query = myObj.data.userName;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('studentHis', 'Teacher_info_u3') + '?' + 'user=' + query;
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-envelope-open-text');
                        link.href = targeturl;
                        var Name = document.createTextNode("ข้อมูลติดต่ออาจารย์");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    } else if (j == 4) {
                        var query = myObj.data.userName;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('studentHis', 'login');
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-sign-out-alt');
                        link.href = targeturl;
                        var Name = document.createTextNode("ออกจากระบบ");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    }
                }
            }
        }
        if (this.readyState != 4 && this.status != 200) {
            alert("Error");
        }
    }
}
