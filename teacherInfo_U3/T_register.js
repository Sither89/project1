function getinfo() {
    
    count = 0;
    teacherUsername_get = document.getElementById('teacherUsername').value;
    teacherphone_get = document.getElementById("teacherphone").value;
    teacheremail_get = document.getElementById("teacheremail").value;
    teachereline_get = document.getElementById("teachereline").value;
    checkinfo();
    
}

function checkinfo() {
    var check = 3;
    var emailcheck = String(teacheremail_get).match(/@/);
    if (teacherUsername_get == '') {
        document.getElementById("teacherUsername").style.borderColor = "red";
        alert('กรุณาใส่ชื่ออาจารย์');
    }
    if (teacherUsername_get != '') {
        document.getElementById("teacherUsername").style.borderColor = "";
        check--;
    }
    if (teacherphone_get.length != 10 && teacherphone_get != '') {
        document.getElementById("teacherphone").style.borderColor = "red";
        alert('กรุณากรอกเบอร์โทรให้ถูกต้อง');
    }
    if (teacherphone_get.length == 10 || teacherphone_get == '') {
        document.getElementById("teacherphone").style.borderColor = "";
        check--;
    }
    if (emailcheck != '@' && teacheremail_get != '') {
        document.getElementById("teacheremail").style.borderColor = "red";
        alert('กรุณากรอกemailให้ถูกต้อง');
    }
    if (emailcheck == '@' || teacheremail_get == '') {
        document.getElementById("teacheremail").style.borderColor = "";
        check--;
    }
    if (check == 0) {
        saveFile();
    }
}

function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

let user = getParameterByName('user');// get value from param 'user'


function saveFile() {
    const xhttp = new XMLHttpRequest();
    xhttp.onload = function () {
        let msg = this.responseText;
        alert('ระบบบันทึกข้อมูลแล้ว');
    }
    let obj = JSON.stringify(teacherUsername_get);
    let obj1 = JSON.stringify(teacherphone_get);
    let obj2 = JSON.stringify(teacheremail_get);
    let obj3 = JSON.stringify(teachereline_get);
    xhttp.open("POST", "/saveTeacher");
    xhttp.send('{"teacherUsername":' + obj + ',"teacherphone":' + obj1 + ',"teacheremail":' + obj2 + ',"teachereline":' + obj3 + '}');
}

function craeteUI() {
    const xmhttp = new XMLHttpRequest();
    xmhttp.onload = function () {
        const myObj = JSON.parse(this.responseText);
        for (let i = 0; i < myObj.length; i++) {
            if (myObj[i].username == user) {
                for (let j = 0; j < 3; j++) {
                    if (j == 0) {
                        var query = myObj[i].username;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('Teacher_register_u3', 'teacherStuList') + '?' + 'user=' + query;
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-envelope-open-text');
                        link.href = targeturl;
                        var Name = document.createTextNode("รายชื่อนักศึกษา");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    }else if(j == 1){
                        var query = myObj[i].username;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('Teacher_register_u3', 'Teacher_register_u3') + '?' + 'user=' + query;
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-user-edit');
                        link.href = targeturl;
                        var Name = document.createTextNode("อาจารย์กรอกข้อมูลติดต่อ");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    }else if(j == 2){
                        var query = myObj[i].username;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('Teacher_register_u3', 'login');
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-sign-out-alt');
                        link.href = targeturl;
                        var Name = document.createTextNode("ออกจากระบบ");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    }
                }
            }
        }
    }
    xmhttp.open("GET", "/getUserTeacher");
    xmhttp.send();
}


function mainpageLogoTeacher(){
    var targeturl = window.location.pathname;
    targeturl = targeturl.replace('Teacher_register_u3', 'Teacher') + '?' + 'user=' + user;
    window.location.href = targeturl;
}