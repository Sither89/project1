function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}
let user = getParameterByName('user');
let id = getParameterByName('id');

function mainpageLogo() {
    var targeturl = window.location.pathname;
    targeturl = targeturl.replace('Teacher_info_u3', 'student') + '?' + 'user=' + user;
    window.location.href = targeturl;
}



function loadDoc() {
    craeteUI();
    const xmhttp = new XMLHttpRequest();
    xmhttp.onload = function () {
        const stdjson = JSON.parse(this.responseText);
        //var div = document.getElementById("con");
        var div = document.createElement('div');
        div.className = 'content';
        var div1 = document.createElement('div');
        div1.className = 'outer';
        var div2 = document.createElement('div');
        div2.className = 'div3';
        var div3 = document.createElement('div');
        div3.className = 'sidebar'
        var div4 = document.createElement('div');
        div4.className = 'container'
        var count = 1;
        for (let i = 0; i < stdjson.length; i++) {
            if (count % 3 == 0) {
                count++;
                div = document.createElement('div');
                div.className = 'content';
                div1 = document.createElement('div');
                div1.className = 'outer';
                div2 = document.createElement('div');
                div2.className = 'div3';
                div3 = document.createElement('div');
                div3.className = 'sidebar'
                div4 = document.createElement('div');
                div4.className = 'container'
                document.getElementsByTagName('body')[0].appendChild(div4).appendChild(div3);
            }
            var input = document.createElement("input");
            var input1 = document.createElement("input");
            var input2 = document.createElement("input");
            var input3 = document.createElement("input");
            var div = document.createElement('div');
            div.className = 'content';
            document.getElementsByTagName('body')[0].appendChild(div2).appendChild(div1).appendChild(div);
            div.appendChild(document.createTextNode("ชื่อ-สกุล "));
            div.appendChild(document.createElement("br"));
            input.disabled = true;
            input.value = "   " + stdjson[i].teacherUsername;
            input.style.fontSize = "x-large";
            input.style.borderRadius = "5px"
            input.style.boxShadow = "2px 2px 6px #FFC60C";
            div.appendChild(input)
            div.appendChild(document.createElement("br"));
            div.appendChild(document.createElement("br"));
            div.appendChild(document.createTextNode("เบอร์ติดต่ออาจารย์ "));
            div.appendChild(document.createElement("br"));
            input1.disabled = true;
            input1.value = "   " + stdjson[i].teacherphone;
            input1.style.fontSize = "x-large";
            input1.style.borderRadius = "5px"
            input1.style.boxShadow = "2px 2px 6px #FFC60C";
            div.appendChild(input1)
            div.appendChild(document.createElement("br"));
            div.appendChild(document.createElement("br"));
            div.appendChild(document.createTextNode("E-mail"));
            div.appendChild(document.createElement("br"));
            input2.disabled = true;
            input2.value = "   " + stdjson[i].teacheremail;
            input2.style.fontSize = "x-large";
            input2.style.borderRadius = "5px"
            input2.style.boxShadow = "2px 2px 6px #FFC60C";
            div.appendChild(input2)
            div.appendChild(document.createElement("br"));
            div.appendChild(document.createElement("br"));
            div.appendChild(document.createTextNode("Line"));
            div.appendChild(document.createElement("br"));
            input3.disabled = true;
            input3.value = "   " + stdjson[i].teachereline;
            input3.style.fontSize = "x-large";
            input3.style.borderRadius = "5px"
            input3.style.boxShadow = "2px 2px 6px #FFC60C";
            div.appendChild(input3)
            div.appendChild(document.createElement("br"));
            div.appendChild(document.createElement("br"));
            div.appendChild(document.createElement("br"));
            div.appendChild(document.createElement("br"));
            count++;
        }
    }
    xmhttp.open("GET", "/getTeacher");
    xmhttp.send();
}


function craeteUI() {
    const xhttp = new XMLHttpRequest();
    let targetUrl = "https://restapi.tu.ac.th/api/v2/profile/std/info/?id=" + user;
    xhttp.open("GET", targetUrl);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.setRequestHeader("Application-Key", "TU2abb8a5ef28d6a54224794d359f3a46fb57bd3b8e1c836fee23ca3b30f91af1480d3e6b4799321ae57be9943904ba18d");
    xhttp.send();
    xhttp.onreadystatechange = function () {
        const myObj = JSON.parse(this.responseText);
        if (this.readyState == 4 && this.status == 200) {
            if (myObj.data.userName == user) {
                for (let j = 0; j < 5; j++) {
                    if (j == 0) {
                        var query = myObj.data.userName;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('Teacher_info_u3', 'enroll') + '?' + 'user=' + query;
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-envelope-open-text');
                        link.href = targeturl;
                        var Name = document.createTextNode("จดทะเบียนล่าช้า");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    } else if (j == 1) {
                        var query = myObj.data.userName;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('Teacher_info_u3', 'studentHis') + '?' + 'user=' + query;
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-history');
                        link.href = targeturl;
                        var Name = document.createTextNode("ประวัติการส่งคำร้อง");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    } else if (j == 2) {
                        var query = myObj.data.userName;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('Teacher_info_u3', 'studentCost') + '?' + 'user=' + query;     //แก้ลิงค์
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-dollar-sign');
                        link.href = targeturl;
                        var Name = document.createTextNode("ภาระค่าใช้จ่าย/ทุน");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    } else if (j == 3) {
                        var query = myObj.data.userName;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('Teacher_info_u3', 'Teacher_info_u3') + '?' + 'user=' + query;
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-envelope-open-text');
                        link.href = targeturl;
                        var Name = document.createTextNode("ข้อมูลติดต่ออาจารย์");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    } else if (j == 4) {
                        var query = myObj.data.userName;
                        var targeturl = window.location.pathname;
                        targeturl = targeturl.replace('Teacher_info_u3', 'login');
                        var parentUL = document.getElementById('link');
                        var parentLI = document.createElement('li');
                        var link = document.createElement('a');
                        var Class = document.createElement('i');
                        Class.setAttribute('class', 'fas fa-sign-out-alt');
                        link.href = targeturl;
                        var Name = document.createTextNode("ออกจากระบบ");
                        link.appendChild(Class);
                        link.appendChild(Name);
                        parentLI.appendChild(link);
                        parentUL.appendChild(parentLI);
                    }
                }
            }
        }
        if (this.readyState != 4 && this.status != 200) {
            alert("Error");
        }
    }
}
