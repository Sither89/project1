var http = require('http');
var fs = require('fs');
var os = require("os");


http.createServer(function (req, res) {
  //console.log(req.url);
  //console.log(req);
  if (req.method === 'GET') {
    var url = '';
    var index = req.url.indexOf('?');
    var path;
    if (index != -1)
      path = req.url.substring(0, index);
    else
      path = req.url;
    //console.log('quary:' + query);
    console.log('path:' + path);
    switch (path) {
      case '/login':                            //Login
        url = 'Login/login.html';
        break;
      case '/student':                          //mianStu
        url = 'main_U6/mainStudent_U6.html';
        break;
      case '/enroll':                           //Form
        url = 'studentForm_U2/studentForm.html';
        break;
      case '/studentHis':                       //ประวัติการส่งคำร้อง
        url = 'studentHis_U7/studentHis1_U7.html';
        break;
      case '/studentHisInfo':                   //รายละเอียดประวัติการส่งคำร้อง
        url = 'studentHis_U7/studentHis2_U7.html';
        break;
      case '/studentCost':                       //ภาระค่าใช้จ่าย/ทุน
        url = 'studentCost_U8/cost_U8.html';
        break;
      case '/Teacher':                          //mainTeacher
        url = 'main_U6/mainTeacher_U6.html';
        break;
      case '/teacherStuList':                   //รายชื่อนักศึกษา
        url = 'Teacher_U4/U4.html';
        break;
      case '/teacherStuInfo':                      //รายละเอียดการส่งคำร้อง
        url = 'Teacher_U4/U4_2.html';
        break;
      case '/getListname':
        url = 'U5/listname.json';
        break;
      case '/getUserTeacher':                   //user pass Teacher
        url = 'res/TeacherUser.json';
        break;
      case '/getTeacher':                   //user pass Teacher
        url = 'teacherInfo_U3/res/teacher.json';
        break;
      case '/getUserStu':
        url = 'res/studentData.json';
        break;
      case '/getSubject':
        url = 'res/subJectList.json';
        break;
      case '/Teacher_register_u3':
        url = 'teacherInfo_U3/Teacher_register.html';        ////////เเก้ไข
        break;
      case '/Teacher_info_u3':
        url = 'teacherInfo_U3/Teacher_info.html';        ////////เเก้ไข
        break;

      case '/exit':
        process.exit();
        break;
      default:
        if (req.url.includes('.')) {
          url = req.url;
          break;
        }
        url = 'Login/redirect.html';
        break;
    }
    if (url) {
      if (url.charAt(0) == '/') {
        url = url.substring(1);
      }
      fs.readFile(url, function (err, data) {
        console.log(err);
        if (err) {
          res.writeHead(404, { 'Content-Type': 'text/html' });
          res.write('<h1>404 NOT FOUND</h1>');
          return res.end();
        } else {
          if (url.endsWith('.html'))
            res.writeHead(200, { 'Content-Type': 'text/html' });
          else if (url.endsWith('.js')) {
            res.writeHead(200, { 'Content-Type': 'text/javascript' });
          } else if (url.endsWith('.json')) {
            res.writeHead(200, { 'Content-Type': 'application/json' });
          } else {
            res.writeHead(200, { 'Content-Type': 'text/plain' });
          }
          res.write(data);
          return res.end();
        }
      });
    } else {
      res.end();
    }
  } else if (req.method === 'POST') {
    var url = '';
    var index = req.url.indexOf('?');
    var path;
    if (index != -1)
      path = req.url.substring(0, index);
    else
      path = req.url;
    console.log('path: ' + path);
    switch (path) {
      case '/saveStudent':
        req.on('data', chunk => {
          let json = JSON.parse(chunk);
          let outputJson = [];
          let data = fs.readFileSync('res/studentData.json', { encoding: 'utf-8' });
          outputJson = JSON.parse(data);
          outputJson.push(json);
          outputJson = JSON.stringify(outputJson);
          fs.writeFileSync('res/studentData.json', outputJson);
          res.writeHead(200, { 'Content-Type': 'text/plain' });
          res.write('ส่งข้อมูลเรียบร้อย');
          res.end();
        })
        break;
      case '/saveTeacher':
        req.on('data', chunk => {
          let json = JSON.parse(chunk);
          let outputJson = [];
          let data = fs.readFileSync('teacherInfo_U3/res/teacher.json', { encoding: 'utf-8' });
          outputJson = JSON.parse(data);
          outputJson.push(json);
          outputJson = JSON.stringify(outputJson);
          fs.writeFileSync('teacherInfo_U3/res/teacher.json', outputJson);
          res.writeHead(200, { 'Content-Type': 'text/plain' });
          res.write('OK');
          res.end();
        })
        break;
      case '/updating':
        req.on('data', chunk => {
          let json = JSON.parse(chunk);
          json = JSON.stringify(json);

          fs.writeFileSync('res/studentData.json', json)
          res.writeHead(200, { 'Content-Type': 'text/plain' });
          res.write('อัพเดทสถานะเรียบร้อย');
          res.end();
        })
        break;
      default: res.end();

    }
  } else {
    res.end();
  }
}).listen(3000);

